# Lab2

## Requirements
- Tested on Android Studio 4.1.2
- Tested on a OnePlus 7T Pro (McLaren Edition) (Android 10)
- Min SDK 26 / Target 30

## Applications

### AuthenticationAPP
This application is the solution from question 1 to 10

### FlickAPP
This application is the solution from question 11 to 34
package fr.clmntw.flickapp.async;

import android.os.AsyncTask;
import android.util.Log;

import androidx.annotation.Nullable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import fr.clmntw.flickapp.adapter.MyAdapter;

import static fr.clmntw.flickapp.utils.StreamUtils.readStream;

/**
 * AsyncTask used to query FlickAPI and fill the adapter of images
 */
public class AsyncFlickrJSONDataForList extends AsyncTask<String, Void, JSONObject> {
    private MyAdapter _adapter;

    public AsyncFlickrJSONDataForList(MyAdapter adapter) {
        this._adapter = adapter;
    }

    @Override
    protected @Nullable
    JSONObject doInBackground(String... strings) {
        // We are returning the first URL that gives us a valid JSON
        for (String string : strings) {
            try {
                Log.i("Papillon", "Starting to process " + string);
                URL url = new URL(string);

                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                try {
                    // Read stream and parse the returned malformed JSON from Flickr
                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());

                    return new JSONObject(readStream(in));
                } finally {
                    urlConnection.disconnect();
                }
            } catch (IOException | JSONException e) {
                Log.e("Papillon", e.getMessage());
            }
        }

        return null;
    }

    @Override
    protected void onPostExecute(JSONObject jsonObject) {
        super.onPostExecute(jsonObject);

        // We got a valid JSON
        if (jsonObject != null) {
            // Logging it
            Log.i("Papillon", jsonObject.toString());

            // Displaying a random image
            try {
                JSONArray jsonPhotos = jsonObject.getJSONObject("photos").getJSONArray("photo");

                for (int i = 0; i < jsonPhotos.length(); i++) {
                    // Querying data from returned JSON
                    JSONObject jsonPhoto = jsonPhotos.getJSONObject(i);

                    // Build the URL
                    String url = "https://live.staticflickr.com/" + jsonPhoto.getString("server") + "/" + jsonPhoto.getString("id") + "_" + jsonPhoto.getString("secret") + "_b.jpg";

                    // Updating the adaptter
                    Log.i("Papillon", "Adding to adapter url : " + url);
                    this._adapter.add(url);
                }

                // Mark the adapter as changed
                this._adapter.notifyDataSetChanged();
            } catch (JSONException e) {
                Log.e("Papillon", e.getMessage());
            }
        }
    }
}

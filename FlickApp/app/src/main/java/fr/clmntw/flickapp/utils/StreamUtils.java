package fr.clmntw.flickapp.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class StreamUtils {
    /**
     * Parse an InputStream and return a String
     * @param in input
     * @return output
     */
    public static String readStream(InputStream in) {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            int i = in.read();
            while (i != -1) {
                byteArrayOutputStream.write(i);
                i = in.read();
            }
            return byteArrayOutputStream.toString();
        } catch (IOException e) {
            return "";
        }
    }
}

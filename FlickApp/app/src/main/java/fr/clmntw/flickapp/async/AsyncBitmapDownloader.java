package fr.clmntw.flickapp.async;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import androidx.annotation.Nullable;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * AsyncTask used to download a remote bitmap resource
 */
public class AsyncBitmapDownloader extends AsyncTask<String, Void, Bitmap> {
    @Override
    protected @Nullable Bitmap doInBackground(String... strings) {
        for (String string: strings) {
            try {
                URL url = new URL(string);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());

                return BitmapFactory.decodeStream(in);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return null;
    }
}

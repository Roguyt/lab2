package fr.clmntw.flickapp.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.preference.PreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.Arrays;

import fr.clmntw.flickapp.async.AsyncBitmapDownloader;
import fr.clmntw.flickapp.async.AsyncFlickrJSONData;
import fr.clmntw.flickapp.R;

/**
 * Default Activity used to query a random image through a Button
 */
public class MainActivity extends AppCompatActivity {
    // Layout elements references
    private Button _getImageBtn;
    private Button _goListActivity;
    private Button _goPreferenceActivity;

    // Click handler
    private final GetImageOnClickListener _getImageOnClickListener = new GetImageOnClickListener(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Recover references
        this._getImageBtn = findViewById(R.id.getImageBtn);
        this._goListActivity = findViewById(R.id.goListActivity);
        this._goPreferenceActivity = findViewById(R.id.goPreferenceActivity);

        // Set events
        this._getImageBtn.setOnClickListener(this._getImageOnClickListener);
        this._goListActivity.setOnClickListener(v -> {
            Intent intent = new Intent(this, ListActivity.class);
            startActivity(intent);
        });
        this._goPreferenceActivity.setOnClickListener(v -> {
            Intent intent = new Intent(this, PreferenceActivity.class);
            startActivity(intent);
        });

        // Verify we have access to GPS data
        // Verify we have the permission
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) !=
                        PackageManager.PERMISSION_GRANTED) {
            // Ask for permissions
            ActivityCompat.requestPermissions(this, new String[] {
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION },
                    1);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.i("Papillon", "requestCode: " + requestCode);
        Log.i("Papillon", "permissions: " + Arrays.toString(permissions));
        Log.i("Papillon", "grantResults: " + Arrays.toString(grantResults));

        if (requestCode == 1) {
            if (grantResults.length == 0 || (grantResults[0] == PackageManager.PERMISSION_DENIED  && grantResults[1] == PackageManager.PERMISSION_DENIED)) {
                // No permissions closing the app
                Toast.makeText(this, "Location permission is required", Toast.LENGTH_SHORT).show();

                finishAffinity();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Properly cancel the async task
        this._getImageOnClickListener.cancel();
    }

    /**
     * onClickListener related to getting a random image from FlickrAPI
     */
    protected static class GetImageOnClickListener implements View.OnClickListener {
        private final MainActivity _ctx;
        private MyAsyncFlickrJSONData _asyncTask = null;

        public GetImageOnClickListener(MainActivity ctx) {
            this._ctx = ctx;
        }

        @Override
        public void onClick(View v) {
            this._asyncTask = new MyAsyncFlickrJSONData(_ctx);

            // Get keyword from Settings
            String keyword = PreferenceManager.getDefaultSharedPreferences(_ctx).getString("keyword", "");

            // Launch and forget
            this._asyncTask.execute("https://www.flickr.com/services/feeds/photos_public.gne?tags=" + keyword + "&format=json&nojsoncallback=1");
        }

        /**
         * Properly cancel the AsyncTask associated
         */
        public void cancel() {
            if (this._asyncTask != null) {
                this._asyncTask.cancel(true);
            }
        }
    }

    /**
     * Override the default AsyncFlickrJSONData to handle this activity use case without losing modularity
     */
    protected static class MyAsyncFlickrJSONData extends AsyncFlickrJSONData {
        private final WeakReference<MainActivity> _ctx;

        public MyAsyncFlickrJSONData(MainActivity ctx) {
            this._ctx = new WeakReference<>(ctx);
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            // Execute default behaviour
            super.onPostExecute(jsonObject);

            // Null checking
            if (jsonObject != null) {
                // Displaying a random image
                try {
                    JSONObject jsonPhoto = jsonObject.getJSONArray("items").getJSONObject(1);
                    String url = jsonPhoto.getJSONObject("media").getString("m");
                    Log.i("Papillon", url);

                    new MyAsyncBitmapDownloader(_ctx.get()).execute(url);
                } catch (JSONException e) {
                    Log.e("Papillon", e.getMessage());
                }
            }
        }
    }

    /**
     * Override the default AsyncBitmapDownloader to handle this activity use case without losing modularity
     */
    protected static class MyAsyncBitmapDownloader extends AsyncBitmapDownloader {
        private final WeakReference<MainActivity> _ctx;

        public MyAsyncBitmapDownloader(MainActivity ctx) {
            this._ctx = new WeakReference<>(ctx);
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            // Default behaviour
            super.onPostExecute(bitmap);

            // Check we do have a bitmap
            if (bitmap != null) {
                // Recover reference or rtn
                MainActivity ctx = _ctx.get();
                if (ctx == null) return;

                ImageView imageView = ctx.findViewById(R.id.imageView);
                imageView.setImageBitmap(bitmap);
            }
        }
    }
}
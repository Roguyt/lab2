package fr.clmntw.flickapp.activity;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.preference.PreferenceManager;

import fr.clmntw.flickapp.R;
import fr.clmntw.flickapp.adapter.MyAdapter;
import fr.clmntw.flickapp.async.AsyncFlickrJSONDataForList;

/**
 * Activity used to display a List of images
 */
public class ListActivity extends AppCompatActivity {
    // Services references
    private LocationManager _locationManager;

    // Layout references
    private ListView _listView;

    // Instantiate the adapter backing the list
    private MyAdapter _myAdapter = new MyAdapter(this);

    // Async references
    private final AsyncFlickrJSONDataForList _asyncTask = new AsyncFlickrJSONDataForList(this._myAdapter);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        setTitle("ListActivity");

        // Recover references
        this._listView = findViewById(R.id.list);

        // Set adapter
        this._listView.setAdapter(this._myAdapter);

        // Get keyword from Settings
        String keyword = PreferenceManager.getDefaultSharedPreferences(this).getString("keyword", "");

        // Get a LocationManager
        this._locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        // Double check we do have the permission
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // No permissions closing the app
            Toast.makeText(this, "Location permission is required", Toast.LENGTH_SHORT).show();

            finishAffinity();

            return;
        }

        // Get last known GPS position
        Location location = this._locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        double latitude = location.getLatitude();
        double longitude = location.getLongitude();

        Log.i("Papillon", "" + latitude + " " + longitude);

        // Load data
        String url = "https://api.flickr.com/services/rest/?" +
                "method=flickr.photos.search" +
                "&license=4" +
                "&api_key=" + PreferenceManager.getDefaultSharedPreferences(this).getString("flickrAPI", "813341dbe4f2bc5e52d09c791bfa10b3") +
                "&tags=" + keyword +
                "&per_page=50&format=json&nojsoncallback=1";
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean("location", false)) {
            url += "&has_geo=1&lat=" + latitude +
                    "&lon=" + longitude;
        }
        // Launch and forget
        this._asyncTask.execute(url);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Properly cancel the async task
        if (this._asyncTask != null) {
            this._asyncTask.cancel(true);
        }
    }
}
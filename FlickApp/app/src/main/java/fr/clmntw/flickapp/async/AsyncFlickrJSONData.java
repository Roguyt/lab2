package fr.clmntw.flickapp.async;

import android.os.AsyncTask;
import android.util.Log;

import androidx.annotation.Nullable;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import static fr.clmntw.flickapp.utils.StreamUtils.readStream;

/**
 * AsyncTask used to query FlickAPI
 */
public class AsyncFlickrJSONData extends AsyncTask<String, Void, JSONObject> {

    @Override
    protected @Nullable JSONObject doInBackground(String... strings) {
        // We are returning the first URL that gives us a valid JSON
        for (String string : strings) {
            try {
                Log.i("Papillon", "Starting to process " + string);
                URL url = new URL(string);

                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                try {
                    // Read stream and parse the returned malformed JSON from Flickr
                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());

                    return new JSONObject(readStream(in));
                } finally {
                    urlConnection.disconnect();
                }
            } catch (IOException | JSONException e) {
                Log.e("Papillon", e.getMessage());
            }
        }

        return null;
    }

    @Override
    protected void onPostExecute(JSONObject jsonObject) {
        super.onPostExecute(jsonObject);

        // We got a valid JSON
        if (jsonObject != null) {
            // Logging it
            Log.i("Papillon", jsonObject.toString());
        }
    }
}

package fr.clmntw.authenticationapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class Authentication extends AppCompatActivity {
    // Layout elements references
    private EditText _loginInput;
    private EditText _passwordInput;
    private Button _authenticationButton;
    private TextView _resultTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authentication);

        // Recover references
        this._loginInput = findViewById(R.id.loginInput);
        this._passwordInput = findViewById(R.id.passwordInput);
        this._authenticationButton = findViewById(R.id.authenticationBtn);
        this._resultTextView = findViewById(R.id.result);

        // Setup click handler
        this._authenticationButton.setOnClickListener(v -> {
            Log.i("Papillon", "Clicked on the authentication button");

            // Start the network request in a separated request
            Thread thread = new Thread(() -> {
                Log.i("Papillon", "Started a thread");

                String textToDisplay = "";

                // Parse the output
                try {
                    String result = authenticateWithHttpBin(_loginInput.getText().toString(), _passwordInput.getText().toString());
                    JSONObject jsonObject = new JSONObject(result);
                    String authenticationStatus = String.valueOf(jsonObject.get("authenticated"));

                    textToDisplay = "Authentication status " + authenticationStatus;
                } catch (JSONException e) {
                    textToDisplay = "An error occurred while requesting data from httpbin.org";
                }

                // Update view
                String finalTextToDisplay = textToDisplay;
                runOnUiThread(() -> _resultTextView.setText(finalTextToDisplay));
            });
            thread.start();
        });
    }

    /**
     * Authenticate using httpbin service
     * @param login input login
     * @param password input password
     * @return a String containing the returned page from httpbin
     */
    private static String authenticateWithHttpBin(String login, String password) {
        try {
            // Setup the URL
            URL url = new URL("https://httpbin.org/basic-auth/bob/sympa");
            // Setup the authentication
            String authorization = "Basic " + Base64.encodeToString((login + ":" + password).getBytes(), Base64.NO_WRAP);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestProperty("Authorization", authorization);

            try {
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());

                // Parse and return
                return readStream(in);
            } finally {
                urlConnection.disconnect();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "";
    }

    /**
     * Parse an InputStream
     * @param in input
     * @return ouput string
     */
    private static String readStream(InputStream in) {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            int i = in.read();
            while(i != -1) {
                byteArrayOutputStream.write(i);
                i = in.read();
            }
            return byteArrayOutputStream.toString();
        } catch (IOException e) {
            return "";
        }
    }
}